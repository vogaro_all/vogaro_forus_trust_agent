"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AddLogo = /*#__PURE__*/function () {
  function AddLogo() {
    _classCallCheck(this, AddLogo);

    this.target = document.querySelector('#footer > .content > a');
    this.parent = document.querySelector('#footer > .content');
  }

  _createClass(AddLogo, [{
    key: "init",
    value: function init() {
      this.createDom();
      console.log(this.parent);
    }
  }, {
    key: "createDom",
    value: function createDom() {
      var ADD_ITEM = document.createElement('a');
      var ADD_ITEM_IMG = document.createElement('img');
      ADD_ITEM.appendChild(ADD_ITEM_IMG);
      ADD_ITEM.setAttribute("id", "link-to-corporate");
      ADD_ITEM.setAttribute("href", "http://www.forus-and.co.jp/");
      ADD_ITEM.setAttribute("target", "_blank");
      ADD_ITEM.style.paddingLeft = '30px';
      ADD_ITEM.style.display = 'inline-block';
      ADD_ITEM.style.height = '47px';
      ADD_ITEM.style.width = '112.69px';
      ADD_ITEM_IMG.setAttribute("src", "images/common/logo.svg");
      ADD_ITEM_IMG.setAttribute("alt", "フォーラス&カンパニー");
      this.parent.insertBefore(ADD_ITEM, this.target);
      console.log(this.parent);
    }
  }]);

  return AddLogo;
}();

var ADD_LOGO = new AddLogo();
ADD_LOGO.init();