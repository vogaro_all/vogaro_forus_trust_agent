<?
/*
	メールフォームチェックファイル
*/


if (!preg_match("/ja/", $_SERVER["HTTP_ACCEPT_LANGUAGE"])) { forbidden(); }

if ($_POST) {
	while (list($key, $var) = each($_POST)) {
		if (is_array($var))
		{
			$i = 0;
			foreach ($var as $v)
			{
				$i++;
				$v = stripslashes($v);
				if ($i > 1)
				{
					$item[$key]["VAR"] .= ', ';
				}
				$item[$key]["VAR"] .= preg_replace("/\r\n\n|\r\n|\n|\r/", "\n", $v);
			}
		}
		else
		{
			$item[$key]["VAR"] = stripslashes($var);
			$item[$key]["VAR"] = preg_replace("/\r\n\n|\r\n|\n|\r/", "\n", $item[$key]["VAR"]);
		}
	}
}


$item_array = file('./init/item.csv');

if ($item_array) {
	foreach ($item_array as $buff) {
		list($title, $name, $check, $spam) = explode(",", $buff);
		$spam = chop($spam);
		$item[$title]["NAME"] = $name;
		$item[$title]["CHECK"] = $check;

		if ($item[$title]["CHECK"] == 1 & $item[$title]["VAR"] == "") { $ERROR[$title] = 1; }
		if ($item[$title]["VAR"] != "" & $spam == 1 & (strlen($item[$title]["VAR"]) == mb_strlen($item[$title]["VAR"], "SJIS"))) { forbidden(); }
	}
}


//入力項目にエラーがある場合
if ($ERROR) {
	$template = implode('', file('./template/index.html'));
	$template = preg_replace('/value="*#(.*?)#"*/ie', '\'value="\'.htmlspecialchars($item["$1"]["VAR"]).\'"\'', $template);

	while (list($key, $var) = each($ERROR)) {
		$template = preg_replace("/#ERROR_" . $key . "#/", '必須項目です！', $template);
	}

	$template = preg_replace("/#ERROR_([^#]*)#/", "", $template);
	$template = preg_replace('/#(.*?)#/ie', 'htmlspecialchars($item["$1"]["VAR"])', $template);
	echo $template;
	exit;
}

else {
	$template = implode('', file('./template/check.html'));
	$template = preg_replace('/#(.*?)#/ie', 'htmlspecialchars($item["$1"]["VAR"])', $template);
	echo $template;
	exit;
}


function forbidden()
{
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>403 Forbidden</title>
</head><body>
<h1>Forbidden</h1>
<p>You don't have permission to access <? echo $_SERVER["REQUEST_URI"]; ?> on this server.</p>
<hr>
<? echo $_SERVER["SERVER_SIGNATURE"]; ?>
</body></html>
<?
	exit;
}

?>
