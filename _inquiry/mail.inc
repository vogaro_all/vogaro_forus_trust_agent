<?
/* メール送信 */

function sendmail($MAIL_TO,$MAIL_FROM,$subject,$body) {

	if(preg_match("/docomo\.ne\.jp$/", $MAIL_TO)){ $carier = 1; }
	elseif(preg_match("/jp-[cdhknqrst]\.ne\.jp$/", $MAIL_TO)){ $carier = 2; }
	elseif(preg_match("/ezweb\.ne\.jp$/", $MAIL_TO) | preg_match("/ido\.ne\.jp$/", $MAIL_TO)){ $carier = 3; }
	else{ $carier = 4; }

	//DoCoMo用
	if($carier == 1){
		$subject = "=?Shift_JIS?B?".base64_encode($subject)."?=";
		$CHARSET = "Shift_JIS";
		$ENCODING = "8bit";
	}
	//J-PHONE用
	elseif($carier == 2){
		mb_convert_variables("JIS", "SJIS", $subject);
		$subject = "=?ISO-2022-JP?B?".base64_encode($subject)."?=";
		mb_convert_variables("JIS", "SJIS", $body);
		$CHARSET = "ISO-2022-JP";
		$ENCODING = "7bit";
	}
	//EZWEB用
	elseif($carier == 3){
		mb_convert_variables("JIS", "SJIS", $subject);
		$subject = "=?ISO-2022-JP?B?".base64_encode($subject)."?=";
		$CHARSET = "Shift_JIS";
		$ENCODING = "8bit";
	}
	//その他用
	else{
		mb_convert_variables("JIS", "SJIS", $subject);
		$subject = "=?ISO-2022-JP?B?".base64_encode($subject)."?=";
		mb_convert_variables("JIS", "SJIS", $body);
		$CHARSET = "ISO-2022-JP";
		$ENCODING = "7bit";
	}

	$mp = popen("/usr/sbin/sendmail -f $MAIL_FROM $MAIL_TO", "w");

	fputs($mp, "MIME-Version: 1.0\n");
	fputs($mp, "From: $MAIL_FROM\n");
	fputs($mp, "To: $MAIL_TO\n");
	fputs($mp, "Subject: $subject\n");

	fputs($mp, "Content-Type: text/plain; charset=".$CHARSET."\n");
	fputs($mp, "Content-Transfer-Encoding: ".$ENCODING."\n");
	fputs($mp, "\n");
	fputs($mp, "$body\n");

	pclose($mp);

}

?>
